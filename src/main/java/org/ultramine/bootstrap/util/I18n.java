package org.ultramine.bootstrap.util;

import org.ultramine.bootstrap.util.Resources;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class I18n
{
	private static final Properties enUs = loadLang("en_US");
	private static Properties selected = enUs;

	public static void select(String locale)
	{
		Properties props = locale.equals("en_US") ? enUs : loadLang(locale);
		selected = props != null ? props : enUs;
	}

	private static Properties loadLang(String locale)
	{
		InputStream inp = Resources.getAsStream("/assets/lang/"+locale+".lang");
		if(inp == null)
			return null;
		try
		{
			Properties props = enUs != null ? new Properties(enUs) : new Properties();
			props.load(inp);
			return props;
		}
		catch(IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	public static String tlt(String key)
	{
		return selected.getProperty(key, key);
	}

	public static String tlt(String key, Object... args)
	{
		return String.format(tlt(key), args);
	}
}
