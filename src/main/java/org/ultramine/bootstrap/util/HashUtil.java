package org.ultramine.bootstrap.util;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashUtil
{
	private static final char[] CHARS = {'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};

	public static MessageDigest getSHA1()
	{
		try
		{
			return MessageDigest.getInstance("SHA-1");
		}
		catch(NoSuchAlgorithmException e)
		{
			throw new RuntimeException("SHA-1 not found", e);
		}
	}

	public static String sha1Str(File file)
	{
		return byteArray2Hex(calculateHash(getSHA1(), file));
	}

	public static byte[] calculateHash(MessageDigest alg, File file)
	{
		DigestInputStream dis = null;
		try
		{
			dis = new DigestInputStream(new FileInputStream(file), alg);

			byte[] buff = new byte[4096];
			do {} while(dis.read(buff) != -1);

			return alg.digest();
		}
		catch(IOException e)
		{
			alg.reset();
			return new byte[0];
		}
		finally
		{
			IOUtils.closeQuietly(dis);
		}
	}

	public static String byteArray2Hex(byte[] hash)
	{
		StringBuilder sb = new StringBuilder(hash.length*2);
		for(int i = 0; i < hash.length; i++)
		{
			byte b = hash[i];
			sb.append(CHARS[(b & 0xff) >> 4]);
			sb.append(CHARS[b & 15]);
		}

		return sb.toString();
	}
}
