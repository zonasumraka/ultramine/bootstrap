package org.ultramine.bootstrap.exceptions;

import org.ultramine.bootstrap.util.I18n;

public class TranslatableMessageException extends RuntimeException
{
	private final String format;
	private Object[] args;

	public TranslatableMessageException(String format)
	{
		super(I18n.tlt(format));
		this.format = format;
	}

	public TranslatableMessageException(String format, Object... args)
	{
		super(I18n.tlt(format, args));
		this.format = format;
		this.args = args;
	}

	public TranslatableMessageException(Throwable cause, String format)
	{
		super(I18n.tlt(format), cause);
		this.format = format;
	}

	public TranslatableMessageException(Throwable cause, String format, Object... args)
	{
		super(I18n.tlt(format, args), cause);
		this.format = format;
		this.args = args;
	}

	public String getTranslatedMessage()
	{
		return args == null ? I18n.tlt(format) : I18n.tlt(format, args);
	}
}
