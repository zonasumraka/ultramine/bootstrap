package org.ultramine.bootstrap.exceptions;

public class ApplicationErrorException extends TranslatableMessageException
{
	public ApplicationErrorException(String format)
	{
		super(format);
	}

	public ApplicationErrorException(String format, Object... args)
	{
		super(format, args);
	}

	public ApplicationErrorException(Throwable cause, String format)
	{
		super(cause, format);
	}

	public ApplicationErrorException(Throwable cause, String format, Object... args)
	{
		super(cause, format, args);
	}
}
