package org.ultramine.bootstrap;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import org.ultramine.bootstrap.maven.MavenDependency;
import org.ultramine.bootstrap.exceptions.ApplicationErrorException;
import org.ultramine.bootstrap.task.DependencyResolver;
import org.ultramine.bootstrap.task.ScriptCreator;
import org.ultramine.bootstrap.util.I18n;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.Set;

public class Main
{
	public static void main(String[] args)
	{
		System.out.println(I18n.tlt("stage.start"));
		OptionParser opt = new OptionParser();
		opt.accepts("help", "Show the help").forHelp();
//		opt.acceptsAll(Arrays.asList("v", "minecraft"), "Version of minecraft (1.7.10)")
//				.withRequiredArg()
//				.ofType(String.class)
//				.defaultsTo("1.7.10")
//				.describedAs("version");
		OptionSpec<String> optLang = opt.accepts("lang", "Language")
				.withRequiredArg()
				.ofType(String.class)
				.defaultsTo(Locale.getDefault().getLanguage() + "_" + Locale.getDefault().getCountry())
				.describedAs("lang");
		opt.accepts("stacktrace", "Print detailed stacktrace on application error");
		OptionSpec<File> optDir = opt.accepts("dir", "Server directory")
				.withRequiredArg()
				.ofType(File.class)
				.defaultsTo(new File("."))
				.describedAs("file");
//		opt.accepts("addliburl", "Add extra libs by URL")
//				.withRequiredArg()
//				.ofType(URL.class)
//				.describedAs("lib url");
		OptionSpec<String> optAddLib = opt.accepts("addlib", "Add extra libs from maven repo (gradle format)")
				.withRequiredArg()
				.ofType(String.class)
				.describedAs("maven lib");
		OptionSpec<URL> optAddRepo = opt.accepts("addrepo", "Add extra maven repository URL")
				.withRequiredArg()
				.ofType(URL.class)
				.describedAs("repo URL");
		opt.accepts("install", "Install ultramine server");
		opt.accepts("update", "Update ultramine server");
		opt.accepts("forge", "Make forge-compatible directory mappings");
		opt.accepts("beta", "Use beta channel");
		OptionSpec<String> optVersion = opt.accepts("version", "ultramine core version")
				.withRequiredArg()
				.ofType(String.class)
				.describedAs("version");

		OptionSpec<String> optXMX = opt.accepts("xmx", "Max heap size for server (-Xmx)")
				.withRequiredArg()
				.ofType(String.class)
				.defaultsTo("2048m")
				.describedAs("max memory");
		OptionSpec<String> optXMS = opt.accepts("xms", "Start heap size for server (-Xms)")
				.withRequiredArg()
				.ofType(String.class)
				.defaultsTo("2048m")
				.describedAs("start memory");

		OptionSpec<String> optTerminal = opt.accepts("terminal", "Terminal (console) mode")
				.withRequiredArg()
				.ofType(String.class)
				.defaultsTo("jline")
				.describedAs("jline/ansi/default/raw");

		OptionSet options;

		try {
			options = opt.parse(args);
		} catch (joptsimple.OptionException e) {
			throw new RuntimeException("Fail to parse command line", e);
		}

		I18n.select(optLang.value(options));

		if (options.has("help")) {
			try {
				opt.printHelpOn(System.out);
				System.exit(0);
			} catch (IOException e) {
				throw new RuntimeException("Fail print help", e);
			}
		}

		try
		{
			File dir = optDir.value(options);
			Set<MavenDependency> deps = DependencyResolver.load(
					dir,
					optVersion.value(options),
					options.has("install") || options.has("update"),
					options.has("beta"),
					optAddRepo.values(options),
					optAddLib.values(options)
			);
			ScriptCreator.create(
					dir,
					deps,
					optXMX.value(options),
					optXMS.value(options),
					optTerminal.value(options),
					options.has("forge")
			);
			System.out.println(I18n.tlt("stage.finished"));
		}
		catch(ApplicationErrorException e)
		{
			System.err.println(e.getTranslatedMessage());
			if(options.has("stacktrace"))
				e.printStackTrace();
			else
				System.err.println(I18n.tlt("hint.stacktrace"));
			System.exit(1);
		}
		catch(RuntimeException e)
		{
			e.printStackTrace();
		}

		System.exit(0);
	}
}
