package org.ultramine.bootstrap;

public class Constants
{
	public static final String UM_REPO = "http://maven.ultramine.ru";
	public static final String UM_CORE_GROUP = "org.ultramine.core";
	public static final String UM_CORE_NAME = "ultramine_core-1.7.10-server";
}
