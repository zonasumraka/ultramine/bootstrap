package org.ultramine.bootstrap.maven;

import org.ultramine.bootstrap.exceptions.ApplicationErrorException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;

public class ProjectObjectModel
{
	private final List<MavenDependency> runtimeDependencies;

	private ProjectObjectModel(String pomUrl)
	{
		Document doc;
		try
		{
			doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(pomUrl);
		}
		catch (Exception e)
		{
			throw new ApplicationErrorException(e, "error.pom", pomUrl);
		}
		NodeList listNodes = doc.getElementsByTagName("dependency");

		runtimeDependencies = new ArrayList<>(listNodes.getLength());
		for(int i = 0, s = listNodes.getLength(); i < s; i++)
		{
			NodeList dependency = listNodes.item(i).getChildNodes();
			String group = null;
			String artifact = null;
			String version = null;
			String scope = null;
			for(int i1 = 0, s1 = dependency.getLength(); i1 < s1; i1++)
			{
				Node nd = dependency.item(i1);
				String name = nd.getNodeName();
				String val = nd.getTextContent();
				switch(name)
				{
				case "groupId": group = val; break;
				case "artifactId": artifact = val; break;
				case "version": version = val; break;
				case "scope": scope = val; break;
				}
			}
			if("runtime".equals(scope) || "compile".equals(scope))
				runtimeDependencies.add(new MavenDependency(group, artifact, version));
		}
	}

	public List<MavenDependency> getRuntimeDependencies()
	{
		return runtimeDependencies;
	}

	public static ProjectObjectModel loadFromXML(String pomUrl)
	{
		return new ProjectObjectModel(pomUrl);
	}

	public static ProjectObjectModel loadFromArtifact(String repoUrl, String group, String project, String version)
	{
		return loadFromXML(repoUrl + "/" + group.replace('.', '/') + "/" + project + "/" + version + "/" + project + "-" + version + ".pom");
	}
}
