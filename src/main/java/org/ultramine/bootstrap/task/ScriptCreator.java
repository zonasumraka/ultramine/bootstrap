package org.ultramine.bootstrap.task;

import org.apache.commons.io.FileUtils;
import org.ultramine.bootstrap.Constants;
import org.ultramine.bootstrap.util.I18n;
import org.ultramine.bootstrap.maven.MavenDependency;
import org.ultramine.bootstrap.exceptions.ApplicationErrorException;
import org.ultramine.bootstrap.util.Resources;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

public class ScriptCreator
{
	public static void create(File dir, Set<MavenDependency> dependencies, String parXmx, String parXms, String parTerminal, boolean forge)
	{
		System.out.println(I18n.tlt("stage.scripts"));
		long xmx = resolveMemory(parXmx);
		long xms = resolveMemory(parXms);
		long xmn = Math.min(xmx / 4, 1024L*1024*1024);
		if(xms > xmx)
			xms = xmx;
		if(xmx < 32*1024*1024)
			throw new ApplicationErrorException("Maximum heap size (-Xmx) is too low: %s", parXmx);

		boolean win = System.getProperty("os.name").toLowerCase().contains("win");
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(
				new File(dir, "ultramine_server_run_line."+(win ? "bat" : "sh")))))
		{
			if(!win)
			{
				writer.write("#!/bin/sh");
				writer.newLine();
			}
			writer.write(Resources.getAsString("/assets/script/java_command_line.txt")
					.replace("{xms}", formatMemory(xmx))
					.replace("{xmx}", formatMemory(xms))
					.replace("{xmn}", formatMemory(xmn))

					.replace("{terminal}", parTerminal)

					.replace("{vanilla_dir}", forge ? "." : "storage")
					.replace("{worlds_dir}", forge ? "." : "worlds")

					.replace("{classpath}", buildClassPath(dependencies))
					.replace('\\', win ? '^' : '\\')
					.replace("#", win ? "::" : "#")
//					.replace("<", win ? "%=" : "`#")
//					.replace(">", win ? "=%" : "`")
			);
			writer.newLine();
		}
		catch(IOException e)
		{
			throw new ApplicationErrorException(e, "error.write.file", "ultramine_server_run_line."+(win ? "bat" : "sh"), e.getMessage());
		}

		try
		{
			FileUtils.writeStringToFile(new File(dir, "start."+(win ? "bat" : "sh")),
					Resources.getAsString("/assets/script/"+(win ? "windows/start.bat" : "shell/start.sh")));
		}
		catch(IOException e)
		{
			throw new ApplicationErrorException(e, "error.write.file", "start."+(win ? "bat" : "sh"), e.getMessage());
		}

		if(forge)
		{
			try
			{
				File serverYml = new File(dir, "settings"+File.separator+"server.yml");
				if(!serverYml.exists())
				{
					String confStr = Resources.getAsString("/assets/server.yml");
					confStr = confStr.replace("{split-world-dirs}", "false");
					Properties defs = new Properties();
					defs.load(Resources.getAsStream("/assets/server.properties"));
					Properties props = new Properties(defs);
					File propsFile = new File(dir, "server.properties");
					if(propsFile.exists())
						try(FileInputStream inp = new FileInputStream(propsFile)) {
							props.load(inp);
						}
					for(Object key : defs.keySet())
						confStr = confStr.replace("{"+key+"}", props.getProperty(key.toString()));
					FileUtils.writeStringToFile(serverYml, confStr);
				}
			}
			catch(IOException e)
			{
				throw new ApplicationErrorException(e, "error.write.file", "settings/server.yml", e.getMessage());
			}
		}
	}

	/** @return number in bytes */
	private static long resolveMemory(String s)
	{
		try
		{
			return Long.parseLong(s);
		}
		catch (NumberFormatException e1)
		{
			long val = Long.parseLong(s.substring(0, s.length() - 1));
			char mod = Character.toLowerCase(s.charAt(s.length()-1));
			if(mod != 'k' && mod != 'm' && mod != 'g')
				throw new IllegalArgumentException(s);
			return val * (mod == 'k' ? 1024 : mod == 'm' ? 1024*1024 : 1024*1024*1024);
		}
	}

	private static String formatMemory(long bytes)
	{
		return (bytes / (1024*1024)) + "m";
	}

	private static String buildClassPath(Set<MavenDependency> dependencies)
	{
		StringBuilder sb = new StringBuilder();
		String separator = System.getProperty("path.separator");

		sb.append(Constants.UM_CORE_NAME+"-latest.jar");
		for(MavenDependency dep : dependencies)
		{
			sb.append(separator);
			sb.append("libraries/");
			sb.append(dep.getArtifactFilename());
		}

		return sb.toString();
	}
}
